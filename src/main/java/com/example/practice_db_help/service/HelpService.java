package com.example.practice_db_help.service;

import com.example.practice_db_help.entity.Help;
import com.example.practice_db_help.entity.Staff;
import com.example.practice_db_help.request.SearchRequest;
import com.example.practice_db_help.response.GetALlRessponse;
import com.example.practice_db_help.response.SearchResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface HelpService {
    List<Help> getAllHelp();

    Page<GetALlRessponse> getAllHelpByManyFields(Pageable pageable);

    Page<SearchResponse> search(String staffCode, String staffName,String tel, String status,
                                String shopId, String helpId, String idNo, Pageable pageable
    );

    Help addHelp(Help h);

    Help update(Help h, String id);

    Help delete(Help h, String id);
}
