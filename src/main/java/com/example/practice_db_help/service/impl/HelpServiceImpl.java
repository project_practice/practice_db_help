package com.example.practice_db_help.service.impl;

import com.example.practice_db_help.entity.Help;
import com.example.practice_db_help.entity.Staff;
import com.example.practice_db_help.repository.HelpRepository;
import com.example.practice_db_help.request.SearchRequest;
import com.example.practice_db_help.response.GetALlRessponse;
import com.example.practice_db_help.response.SearchResponse;
import com.example.practice_db_help.service.HelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HelpServiceImpl implements HelpService {

    @Autowired
    private HelpRepository helpRepository;

    @Override
    public List<Help> getAllHelp() {
        return helpRepository.findAll();
    }

    @Override
    public Page<GetALlRessponse> getAllHelpByManyFields(Pageable pageable) {
        return helpRepository.getALlHelp(pageable);
    }

    @Override
    public Page<SearchResponse> search(String staffCode, String staffName, String tel, String status,
                                       String shopId, String helpId, String idNo, Pageable pageable) {
        return helpRepository.search(staffCode, staffName, tel, status, shopId, helpId, idNo, pageable);
    }

    @Override
    public Help addHelp(Help h) {
        Help newHelp = new Help();
        newHelp.setHelpName(h.getHelpName());
        newHelp.setContetn(h.getContetn());
        newHelp.setParentHelpId(h.getParentHelpId());
        newHelp.setCreatedUser(h.getCreatedUser());
        newHelp.setPosition(h.getPosition());
        newHelp.setStatus(h.getStatus());
        newHelp.setHelpType(h.getHelpType());
        helpRepository.save(newHelp);
        return newHelp;
    }

    @Override
    public Help update(Help h, String id) {
        Optional<Help> newHelp = helpRepository.findById(id);
        newHelp.get().setHelpName(h.getHelpName());
        newHelp.get().setContetn(h.getContetn());
        newHelp.get().setParentHelpId(h.getParentHelpId());
        newHelp.get().setCreatedUser(h.getCreatedUser());
        newHelp.get().setPosition(h.getPosition());
        newHelp.get().setStatus(h.getStatus());
        newHelp.get().setHelpType(h.getHelpType());
        helpRepository.save(newHelp.get());
        return newHelp.get();
    }

    @Override
    public Help delete(Help h, String id) {
        Optional<Help> newHelp = helpRepository.findById(id);
        newHelp.get().setStatus("0");
        helpRepository.save(newHelp.get());
        return newHelp.get();
    }
}
