package com.example.practice_db_help.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "helpp")
@Builder

public class Help {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "helpp_seqp_generator")
    @SequenceGenerator(name = "helpp_seqp_generator", sequenceName = "helpp_seqq", allocationSize = 1)
    private Long helpId;

    @Column(name = "help_name")
    private String helpName;

    @Column(name = "parent_help_id")
    private Integer parentHelpId;

    @Column(name = "help_type")
    private String helpType;

    @Column(name = "position")
    private String position;

    @Column(name = "contetn")
    private String contetn;

    @Column(name = "status")
    private String status;

    @Column(name = "created_user")
    private String createdUser;

}
