package com.example.practice_db_help.repository;

import com.example.practice_db_help.entity.Help;
import com.example.practice_db_help.entity.Staff;
import com.example.practice_db_help.response.GetALlRessponse;
import com.example.practice_db_help.response.SearchResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HelpRepository extends JpaRepository<Help, String> {

    @Query(value = """
                select s.staff_id, h.parent_help_id, h.status
                , h.help_type, s.shop_id
                from staff s,  helpp h
                where h.created_user = s.staff_code
            """, countQuery = """
            select COUNT (s.staff_id)
                from helpp h, staff s 
                where h.created_user = s.staff_code
                """, nativeQuery = true)
    Page<GetALlRessponse> getALlHelp(Pageable pageable);

    @Query(value = """
                select distinct s.staff_id, s.staff_name, s.tel, s.address, s.shop_id, s.id_no, s.status 
                from staff s left join helpp h on h.created_user = s.staff_code
                where (:staffCode is null or s.staff_code like '%' || :staffCode || '%')
                      and (:staffName is null or s.staff_name like '%' || :staffName || '%')
                      and (:tel is null or s.tel like '%' || :tel || '%')
                      and (:status is null or s.status like '%' || :status || '%')
                      and (:shopId is null or s.shop_id like '%' || :shopId || '%')
                      and (:helpId is null or h.help_id like '%' || :helpId || '%')
                      and (:idNo is null or s.id_no like '%' || :idNo || '%')
            """, countQuery = """
                                select COUNT (s.staff_id)
                                    from staff s left join helpp h on h.created_user = s.staff_code
                                    where (:staffCode is null or s.staff_code like '%' || :staffCode || '%')
                                  and (:staffName is null or s.staff_name like '%' || :staffName || '%')
                                  and (:tel is null or s.tel like '%' || :tel || '%')
                                  and (:status is null or s.status like '%' || :status || '%')
                                  and (:shopId is null or s.shop_id like '%' || :shopId || '%')
                                  and (:helpId is null or h.help_id like '%' || :helpId || '%')
                                  and (:idNo is null or s.id_no like '%' || :idNo || '%')
            """, nativeQuery = true)
    Page<SearchResponse> search(@Param("staffCode") String staffCode,
                                @Param("staffName") String staffName,
                                @Param("tel") String tel,
                                @Param("status") String status,
                                @Param("shopId") String shopId,
                                @Param("helpId") String helpId,
                                @Param("idNo") String idNo,
                                Pageable pageable);

}
