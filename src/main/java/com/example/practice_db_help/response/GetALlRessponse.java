package com.example.practice_db_help.response;

import com.example.practice_db_help.entity.Help;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(types = {Help.class})

public interface GetALlRessponse {

    @Value("#{target.staff_id}")
    Integer getStaffId();

    @Value("#{target.parent_help_id}")
    String getParentHelpId();

    @Value("#{target.status}")
    String getStatus();

    @Value("#{target.help_type}")
    String getHelpType();

    @Value("#{target.shop_id}")
    Integer getShopId();
}
