package com.example.practice_db_help.response;

import com.example.practice_db_help.entity.Staff;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(types = {Staff.class})
public interface SearchResponse {

    @Value("#{target.staff_id}")
    Integer getStaffId();

    @Value("#{target.staff_name}")
    String getStaffName();

    @Value("#{target.tel}")
    String getTel();

    @Value("#{target.address}")
    String getAddress();

    @Value("#{target.shop_id}")
    Integer getShopId();

    @Value("#{target.id_no}")
    String getIdNo();

    @Value("#{target.status}")
    String getStatus();
}