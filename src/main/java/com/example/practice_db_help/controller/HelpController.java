package com.example.practice_db_help.controller;

import com.example.practice_db_help.common.ResponseObject;
import com.example.practice_db_help.entity.Help;
import com.example.practice_db_help.request.SearchRequest;
import com.example.practice_db_help.response.GetALlRessponse;
import com.example.practice_db_help.response.SearchResponse;
import com.example.practice_db_help.service.HelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/help")
public class HelpController {

    @Autowired
    private HelpService helpService;

    @GetMapping("/view")
    public ResponseObject getAll() {
        return new ResponseObject(helpService.getAllHelp());
    }

    @GetMapping("/get-all")
    public ResponseObject getAllByManyFields(@PageableDefault(size = 2, page = 0, sort = "staff_id",
            direction = Sort.Direction.ASC) Pageable pageable) {
        Page<GetALlRessponse> results = helpService.getAllHelpByManyFields(pageable);
        return new ResponseObject(results.getContent());
    }

    @GetMapping("/search")
    public ResponseObject search(@RequestParam(required = false) String staffCode,
                                 @RequestParam(required = false) String staffName,
                                 @RequestParam(required = false) String tel,
                                 @RequestParam(required = true) String status,
                                 @RequestParam(required = false) String shopId,
                                 @RequestParam(required = false) String helpId,
                                 @RequestParam(required = false) String idNo,
                                 @PageableDefault(size = 2, page = 0) Pageable pageable) {
        Page<SearchResponse> searchResults = helpService.search(staffCode, staffName, tel, status, shopId, helpId, idNo, pageable);
        return new ResponseObject(searchResults.getContent());
    }

    @PostMapping("/add")
    public ResponseObject add(@RequestBody Help help) {
        return new ResponseObject(helpService.addHelp(help));
    }

    @PutMapping("/update/{id}")
    public ResponseObject update(@RequestBody Help help, @PathVariable("id") String id) {
        return new ResponseObject(helpService.update(help, id));
    }

    @PutMapping("/delete/{id}")
    public ResponseObject delete(@RequestBody Help help, @PathVariable("id") String id) {
        return new ResponseObject(helpService.delete(help, id));
    }
}
