package com.example.practice_db_help.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchRequest {
    private String staffCode;
    private String staffName;
    private String tel;
    private String status;
    private Integer shopId;
    private Integer helpId;
    private String idNo;
}
